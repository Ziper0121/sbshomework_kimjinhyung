#include <stdio.h>
#include <string.h>

//사람
class person{
protected :
	char name[100];//이름
	int age;//나이
	char gender;//성별
public :
	person(char* _name, int _age,char gender);

	char* get_name();
	int get_age();
	char get_gender();
};

// 아이
class child : public person{
public :
	child(char*_name, int _age, char gender) : person(name, age, gender){// person(name, age, gender) 의 맴버로 변하게 된다.
	}
};

//성인
class Adult : public person{
public:
	Adult(char*_name, int _age, char gender) : person(name, age, gender){
	}
};

//학생
class surdent : public child {
private :
	int grade;//학년
	char* schooIName;//학교 이름
	char C_J_G;//초,중,고
public :
	surdent(char*_name, int _age, char gender) : child(name, age, gender){
	}
	void R_grade(int _grade);
	void R_schooIName(char* _sName);
	void receive_C_J_G(char _CJG );
	int get_gtade();
	char* get_schooIName();
	char get_C_J_G();
};

//대학생
class UniversityStudent : public Adult{
private:
	int grade;//학년
	char schoolName[100];//학교 이름
public:
	UniversityStudent(char*_name, int _age, char gender) : Adult(name, age, gender){
	}
	void R_grade(int _grade);
	void R_schooIName(char* _sName);

	int get_gtade();
	char* get_schooIName();
};

class Worker : public Adult{
private :
	char* rectalName;//직장명
	int salaty;//봉급
public:
	Worker(char*_name, int _age, char gender) : Adult(name, age, gender){}
	void R_rectalName(char* rectalName);
	void R_salaty(int _salaty);
	char* get_rectalName();
	int get_salaty();
};