#include <stdio.h>
#include <string.h>

//네오버
class Profile {
private:
	char* id;
	char* password;

	char* name;
	char* Email;
public:
	void Riceive_id(char* _id);
	void Riceive_password(char* _password);
	void Riceive_name(char* _name);
	void Riceive_Email(char* _Email);

	char* print_id();
	char* print_password();
	char* print_name();
	char* print_Email();
};
//일반회원
class Normal : public Profile{
private:
	int date_of_birth;//생년월일
public:
	void Riceive_date_of_birth(int _date_of_birth);
	int print_date_of_birth();
};
//기업회원
class Enterprise : public Profile{
private:
	int buisnessman;//사업자번호
public:
	void Riceive_buisnessman(int _buisnessman);
	int print_buisnessman();
};
//네오버관리자
class manager : public Profile{
private:
	int temple;//사원번호
public:
	void Riceive_temple(int _temple);
	int print_temple();
};