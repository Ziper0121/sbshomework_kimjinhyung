#include<stdio.h>
#include <string.h>

//재고 
class Stock{
private :
	char* company;//회사
protected :
	int price;//가격
public :
	void Receive_price(int _price);
	void Receive_Company(char* _companny);
	int Prinf_price();
	char* Print_Company();
};

//사이즈
class Size : public Stock{
protected :
	char dimansions;//치수
public:
	virtual void Receive_Dimansions(char _dimansions);
	char Print_Dimansions();
};

//남자
class Man : public Size{
private:
	char* gender="Man";
public:
	char* Print_Gender();
};

//여자
class Female : public Size{
private :
	char* gender="Female";
public:
	char* Print_Gender();
};

//////////////////////////////////////사이즈 없음////////////////////////////////////

//양말
class Socks : public Stock{};
//////////////////////////////////////남녀 공용////////////////////////////////////// 

//티셔츠
class Tshirt : public Size{};
//바지
class pants : public Size{};
//신발
class shoes : public Size{};
//팬티? 남녀 공용?
class panties : public Size{};

//////////////////////////////////////남자 전용//////////////////////////////////////
class Running : public Man{}; //런닝
//////////////////////////////////////여성 전용//////////////////////////////////////
class Bra : public Female{
public :
	virtual void Receive_Dimansions(char _dimansions);
};
class stocking : public Female{};

///////////////////////////////////////scanf정수 입력받기///////////////////////////////////////

int intscanf(char* youprintf);

//////////////////////////////////////유니클로/////////////////////////////////////////
class Umiqlo {
private:
	int capital = 20000000;              //자본금 2천만원
	int revenue=0;                         //수익
public:
	Stock* Qunantity[161];            //수량
	Umiqlo(){
		for (int i = 0; i < 20; i++){
			Qunantity[i] = new Socks();
			Qunantity[i + 20] = new Tshirt();
			Qunantity[i + 40] = new pants();
			Qunantity[i + 60] = new shoes();
			Qunantity[i + 80] = new panties();
			Qunantity[i + 100] = new Running();
			Qunantity[i + 120] = new Bra();
			Qunantity[i + 140] = new stocking();
		}
	}
	int print_capital();
	int print_revenue();
};