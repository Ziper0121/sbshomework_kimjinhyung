#include <map>

//캐릭터 포인터 set <map>
#define PC_MAP std::map<std::string,std::string>
//유저 정보
class User_Information{
protected:

	//동일한 아이디를 생성할수 없게 map을 사용한다.
	//유저의 이이디를 저장하는 장소
	PC_MAP server_id_information;

	//글을 입력받는 곳
	//글의 제목과 그 안에 내용을 받는 곳
	PC_MAP server_Article_information;
	PC_MAP::iterator server_Article;


	//글 제목
	std::string title = server_Article->second;
	//글 몸체
	std::string article_Body = server_Article->first;

};

//로그인 화면
class Login :public User_Information{
public:

	// 로그인과 회원가입을 나누고 그것을 구동시키는 함수 정보의 따라 0과 1을 리턴하여 회원과 운영자를 나눈다. 0 = 회원  1 = 운영자
	bool Login_Screen();
	//직접적으로 로그인을 받는 함수 0과 1을 리턴하여 회원과 운영자를 나눈다. 0 = 회원  1 = 운영자
	bool get_Login();

	//회원가입
	void Get_Id_Password_Sign_Up();
};

//메뉴 화면
class Menu:public Login{
public:

	//메인 화면
	void Menu_Screen();

	//글쓰기
	void Writing();
	//글읽기
	void Read_Article();
	//어드민 글 삭제
	void Deleting_Posts();

	//글삭제
	void Written_Menu();
};

//scanf포인터를 return하는 함수
char* PC_Scanf(char* print);

//scanf를 return하는 함수
char C_Scanf(char* print);

//scamf를 return하는 함수
int I_Scnaf(char* print);