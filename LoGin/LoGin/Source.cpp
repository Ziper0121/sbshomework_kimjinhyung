#include"Header.h"
#include <stdio.h>
#include<string.h>


/////////////////////////////////////////로그인 화면/////////////////////////////////////////


// 로그인과 회원가입을 나누고 그것을 구동시키는 함수 정보의 따라 0과 1을 리턴하여 회원과 운영자를 나눈다. 0 = 회원  1 = 운영자
bool Login::Login_Screen(){
	//회원가입을 할경우 로그인을 해야하기에 continue를 이용해 다시 진입
	while (int end = 1){
		switch (C_Scanf("당신이 원하는 행동에 해당하는 번호를 적어주세요 \n [ 1 ]로그인    [ 2 ] 회원가입 \n")){
		case 1:
			//bool이기 때문에 1또는 0을 return
			return Login::get_Login();
			break;
		case 2:
			Login::Get_Id_Password_Sign_Up();
			continue;
		}
	}
}


/////////////////////////////////////////로그인/////////////////////////////////////////


// 로그인의 정보의 따라 0과 1을 리턴하여 회원과 운영자를 나눈다. 0 = 회원  1 = 운영자
bool Login::get_Login(){
	server_id_information;

	//마스터 ID
	server_id_information.insert("manager1", "MASTER1234");

	//ID를 비교하기 위한 MAP 
	PC_MAP get_id;

	while (int end = 1){

		//입력한 ID가 등록이 되있을 경우와 없을 경우
		switch (server_id_information.insert == get_id.insert(PC_Scanf("ID : "), PC_Scanf("Password : "))){

		case '0':
			//저장된 ID이 아닐경우 재입력 요구
			printf("ID 가 틀렸거나 PASSWORD가 틀렸습니다.\n 다시 입력 바랍니다. \n");
			continue;
			
		case '1':
		//등록된 ID일 경우 운영자와 일반 회원인지 분별  0 = 회원  1 = 운영자
		//입력한 아이디가 운영자일경우 1을 리턴
			switch (get_id.insert == "manager1" && get_id.insert == "MASTER1234"){
			case '1':
				return 1;
				break;
			}
			return 0;
			break;
		}
	}
}


/////////////////////////////////////////회원가입/////////////////////////////////////////


void Login::Get_Id_Password_Sign_Up(){

	//아이디와 비번을 입력받는 함수 아마 동일한 아이디가 있을경우 막는 걸로 암

	server_id_information.insert
		(PC_Scanf("원하는 ID를 입력해주세요\n ID : ")
		,PC_Scanf("원하는 PASSWORD를 입력해주세요\n PASSWORD : "));
}


/////////////////////////////////////////글/////////////////////////////////////////


//글쓰기
void Menu::Writing(){
	server_Article = server_Article_information.begin();
	Menu::title = PC_Scanf(" 글 제목  ");
	Menu::article_Body = PC_Scanf("");
}

//글읽기
void Menu::Read_Article(){
	server_Article = server_Article_information.begin();
	//제목을 불르는 것
	for (; server_Article != server_Article_information.end; ++server_Article){
		printf("%s",title);
	}
	//원하는 위치에 글을 읽기 ???????????????????????????????????????????????????????????????????
	printf("%s", server_Article_information[I_Scnaf("원하는 글 위치를 적어주세요. : ")].c_str());
}

//글삭제
void Menu::Deleting_Posts(){
	server_Article_information;//원하는 위치를 입력해서 밸류를 한다.위치 찾는게 뭔지 모르것다.
}



/////////////////////////////////////////메인/////////////////////////////////////////

//글메뉴
void Menu::Menu_Screen(){
	while (1){
		switch (Login::Login_Screen()){
		case '0':
			switch (C_Scanf("원하는 행동의 말해주십시오 : \n [1] 글쓰기 [2] 글읽기 [3] 나가기 ")){
			case '1':
				Menu::Writing();
				break;
			case '2':
				Menu::Read_Article();
				break;
			case '3':
				return;
				break;
			}
			break;
		case'1':
			switch (C_Scanf("원하는 행동의 말해주십시오 : \n [1] 글쓰기 [2] 글읽기 [3] 나가기 [4] 글삭제 ")){
			case '1':
				Menu::Writing();
				break;
			case '2':
				Menu::Read_Article();
				break;
			case '3':
				return;
				break;
			case '4':
				Menu::Deleting_Posts();
				break;
			}
			break;
		}
	}
}

int main(){
	Menu article;
	article.Menu_Screen();
	return 0;
}

//캐릭터 포인터
char* PC_Scanf(char* print){
	char* pc_return;
	printf(print);
	fflush(stdin);
	scanf("%s", pc_return);
	return pc_return;
}
//캐릭터
char C_Scanf(char* print){
	char c_return;
	printf(print);
	fflush(stdin);
	scanf("%c", &c_return);
	return c_return;
}
//정수
int I_Scnaf(char* print){
	int i_scanf;
	printf(print);
	fflush(stdin);
	scanf("%d", &i_scanf);
	return i_scanf;
}